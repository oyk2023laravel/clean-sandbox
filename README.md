<h1 align="center">
  Clean Sandbox Backend
  Hexagonal Architecture, DDD & CQRS in Laravel PHP
</h1>

<p align="center">
    <a href="https://laravel.com/"><img src="https://img.shields.io/badge/Laravel-9-FF2D20.svg?style=flat-square&logo=laravel" alt="Laravel 9"/></a>
    <a href="https://www.php.net/"><img src="https://img.shields.io/badge/PHP-8.1-777BB4.svg?style=flat-square&logo=php" alt="PHP"/></a>
    <a href="https://www.docker.com/"><img src="https://img.shields.io/badge/docker-3-2496ED.svg?style=flat-square&logo=docker" alt="Docker"/></a>
</p>

<p align="center">
  This is a draft, sandbox repo containing a <strong>Laravel application</strong> using <strong>Domain-Driven Design (DDD)</strong> with <strong>Command Query Responsibility Segregation (CQRS)</strong> principles in <strong>Hexagonal Architecture</strong>.
  <br />
  <br />
  The main objective of this implementation is to use Laravel as backend framework but instead of using MVC architecture, go for DDD and Hexagonal for less "domain complexity". 
  <br />
  <br />
</p>

## Installation

```
$ git clone git@gitlab.com:oyk2023laravel/clean-sandbox.git
$ cd clean-sandbox
$ cp .env.example .env
$ docker run --rm \
    --pull=always \
    -v "$(pwd)":/opt \
    -w /opt \
    laravelsail/php82-composer:latest \
    bash -c "composer install && php artisan key:generate"
$ sail up -d
$ sail artisan migrate
```

## Testing

```
$ curl --request POST \
  --url http://localhost/api/users/register \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --data '{
	"name": "Eray",
	"email": "erayaydinn@protonmail.com",
	"password": "12345678",
	"password_confirmation": "12345678"
}'
```

```
{
	"user": {
		"id": "b871a13a-6a9d-41da-af24-f9eacb888b47"
	}
}
```

## Project Structure

### Src

`src` is for "Source". Here we put all our code base being as independent as possible of any implementation (except `infrastructure` sub-folder).

The main idea is to use this as our **pure** code, with no vendor, just domain logic.

### Bounded Contexts

**User:** Where the main functionality is implemented for the User scope. Registration, listing, deleting, etc...

**Place:** where the main functionality is implemented for the Place scope. Listing, claiming the place, etc...

```bash
$ tree -L 2 src

src
├── Place
│   ├── Application
│   ├── Domain
│   ├── Infrastructure
│   └── Interface
└── User
    ├── Application
    ├── Domain
    ├── Infrastructure
    └── Interface
 
$ tree -L 2 shared

shared
├── Application
├── Domain
├── Infrastructure
└── Interface
```

### Repository Pattern

In current implementation, our repositories try to be as simple as possible.

### CQRS

`Symfony Messenger` has been used to implement commands, queries and domain events.

### Functional Programming

`phunctional` has been used for some HoF in the bus implementation.

