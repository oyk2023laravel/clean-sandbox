<?php

namespace Acme\Place\Interface\Http\Controllers;

use Acme\Place\Application\Listing\AllPlacesQuery;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Shared\Interface\Http\Controllers\Controller;

class AllPlacesController extends Controller
{
    public function __invoke(Request $request): JsonResponse
    {
        $placesResponse = $this->askQuery(
            new AllPlacesQuery()
        );

        return response()->json([
            'places' => $placesResponse->toArray()
        ]);
    }
}
