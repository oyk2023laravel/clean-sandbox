<?php

namespace Acme\Place\Interface\Http\Controllers;

use Acme\Place\Application\Create\CreatePlaceCommand;
use Acme\Place\Interface\Http\Requests\PlaceCreateRequest;
use Illuminate\Http\JsonResponse;
use Shared\Interface\Http\Controllers\Controller;

class CreatePlaceController extends Controller
{
    public function __invoke(PlaceCreateRequest $request): JsonResponse
    {
        $id = $this->generateUuid();

        $this->dispatchCommand(
            new CreatePlaceCommand(
                $id,
                $request->get('name')
            )
        );

        return response()->json(['place' => ['id' => $id]], 201);
    }
}
