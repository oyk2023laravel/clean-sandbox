<?php

declare(strict_types=1);

namespace Acme\Place\Application;

use Acme\Place\Domain\Place;
use Acme\Place\Domain\Places;
use Shared\Domain\Bus\Query\Response;
use function Lambdish\Phunctional\map;

final class PlacesResponse implements Response
{
    /**
     * @param array<PlaceResponse> $places
     */
    public function __construct(private array $places)
    {
    }

    public static function from(Places $places): self
    {
        return new self(
            map(
                fn (Place $place) => PlaceResponse::from($place),
                $places->all()
            )
        );
    }

    public function toArray(): array
    {
        return map(
            fn (PlaceResponse $response) => $response->toArray(),
            $this->places
        );
    }
}
