<?php

declare(strict_types=1);

namespace Acme\Place\Application\Listing;

use Acme\Place\Application\PlacesResponse;
use Acme\Place\Domain\PlaceRepository;
use Shared\Domain\Bus\Query\QueryHandler;

final class AllPlacesQueryHandler implements QueryHandler
{
    public function __construct(private PlaceRepository $repository) {}

    public function __invoke(AllPlacesQuery $query): PlacesResponse
    {
        return PlacesResponse::from($this->repository->all());
    }
}
