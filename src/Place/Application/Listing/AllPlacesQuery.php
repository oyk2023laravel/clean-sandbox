<?php

declare(strict_types=1);

namespace Acme\Place\Application\Listing;

use Shared\Domain\Bus\Query\Query;

final class AllPlacesQuery implements Query
{}
