<?php

declare(strict_types=1);

namespace Acme\Place\Application;

use Acme\Place\Domain\Place;
use JetBrains\PhpStorm\Pure;
use Shared\Domain\Bus\Query\Response;

final class PlaceResponse implements Response
{
    public function __construct(private string $id, private string $name) {}

    #[Pure]
    public static function from(Place $place): self
    {
        return new self(
            $place->id()->value(),
            $place->name()->value()
        );
    }

    public function id(): string
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    /** @noinspection PhpArrayShapeAttributeCanBeAddedInspection */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
