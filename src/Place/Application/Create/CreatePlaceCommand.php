<?php

declare(strict_types=1);

namespace Acme\Place\Application\Create;

use Shared\Domain\Bus\Command\Command;

final class CreatePlaceCommand implements Command
{
    public function __construct(private string $id, private string $name) {}

    public function id(): string
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }
}
