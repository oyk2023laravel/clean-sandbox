<?php

declare(strict_types=1);

namespace Acme\Place\Application\Create;

use Acme\Place\Domain\Place;
use Acme\Place\Domain\PlaceAlreadyExists;
use Acme\Place\Domain\PlaceId;
use Acme\Place\Domain\PlaceName;
use Acme\Place\Domain\PlaceRepository;
use Shared\Domain\Bus\Command\CommandHandler;
use Shared\Domain\Bus\Event\EventBus;

final class CreatePlaceCommandHandler implements CommandHandler
{
    public function __construct(
        private PlaceRepository $repository,
        private EventBus $bus
    ) {}

    /**
     * @throws PlaceAlreadyExists
     */
    public function __invoke(CreatePlaceCommand $command): void
    {
        $id = PlaceId::from($command->id());
        $place = $this->repository->find($id);

        if ($place !== null) {
            throw new PlaceAlreadyExists();
        }

        $name = PlaceName::from($command->name());
        $place = Place::create($id, $name);
        $this->repository->save($place);
        $this->bus->publish(...$place->pullDomainEvents());
    }
}
