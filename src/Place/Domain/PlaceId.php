<?php

declare(strict_types=1);

namespace Acme\Place\Domain;

use Shared\Domain\ValueObject\UuidValueObject;

final class PlaceId extends UuidValueObject
{
}
