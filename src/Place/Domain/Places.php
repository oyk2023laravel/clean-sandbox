<?php

declare(strict_types=1);

namespace Acme\Place\Domain;

use Shared\Domain\Collection\Collection;

final class Places extends Collection
{
    protected function type(): string
    {
        return Place::class;
    }
}
