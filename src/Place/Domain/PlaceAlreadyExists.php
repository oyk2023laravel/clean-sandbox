<?php

declare(strict_types=1);

namespace Acme\Place\Domain;

use JetBrains\PhpStorm\Pure;
use Shared\Domain\Exceptions\DomainException;
use Throwable;

final class PlaceAlreadyExists extends DomainException
{
    #[Pure]
    public function __construct(string $message = "Place already exists", int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
