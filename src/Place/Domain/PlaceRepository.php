<?php

declare(strict_types=1);

namespace Acme\Place\Domain;

interface PlaceRepository
{
    public function find(PlaceId $id): ?Place;

    public function all(): Places;

    public function save(Place $place): void;

    public function delete(PlaceId $id): void;
}
