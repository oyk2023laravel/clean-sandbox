<?php

declare(strict_types=1);

namespace Acme\Place\Domain;

use Shared\Domain\Bus\Event\DomainEvent;

final class PlaceWasCreated extends DomainEvent
{
    public function __construct(private string $id, private string $name, ?string $eventId = null, ?string $occurredOn = null)
    {
        parent::__construct($id, $eventId, $occurredOn);
    }

    public static function from(string $id, array $body, string $eventId, string $occurredOn): DomainEvent
    {
        return new self($id, $body['name'], $eventId, $occurredOn);
    }

    public static function eventName(): string
    {
        return 'place.was_created';
    }

    /**
     * @return array
     * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
     */
    public function to(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }

    public function id(): string
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }
}
