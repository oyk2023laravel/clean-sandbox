<?php

declare(strict_types=1);

namespace Acme\Place\Domain;

use Shared\Domain\ValueObject\StringValueObject;

final class PlaceName extends StringValueObject
{
}
