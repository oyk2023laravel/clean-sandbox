<?php

declare(strict_types=1);

namespace Acme\Place\Domain;

use Shared\Domain\Aggregate\AggregateRoot;

final class Place extends AggregateRoot
{
    public function __construct(
        private readonly PlaceId $id,
        private readonly PlaceName $name
    ) {}

    public static function from(string $id, string $name): self
    {
        return new self(
            PlaceId::from($id),
            PlaceName::from($name)
        );
    }

    public static function create(PlaceId $id, PlaceName $name): self
    {
        $place = new self($id, $name);
        $place->record(new PlaceWasCreated($id->value(), $name->value()));

        return $place;
    }

    public function id(): PlaceId
    {
        return $this->id;
    }

    public function name(): PlaceName
    {
        return $this->name;
    }
}
