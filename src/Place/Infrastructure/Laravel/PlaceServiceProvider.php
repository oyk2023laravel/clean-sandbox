<?php

declare(strict_types=1);

namespace Acme\Place\Infrastructure\Laravel;

use Acme\Place\Application\Create\CreatePlaceCommandHandler;
use Acme\Place\Application\Listing\AllPlacesQueryHandler;
use Acme\Place\Domain\PlaceRepository;
use Acme\Place\Infrastructure\Persistence\Eloquent\PlaceRepository as EloquentPlaceRepository;
use Acme\Place\Interface\Http\Controllers\AllPlacesController;
use Acme\Place\Interface\Http\Controllers\CreatePlaceController;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\RateLimiter;
use Shared\Infrastructure\Laravel\DomainServiceProvider;
use Shared\Infrastructure\Laravel\HasMigrations;
use Shared\Infrastructure\Laravel\HasRoutes;

class PlaceServiceProvider extends DomainServiceProvider
{
    use HasRoutes, HasMigrations;

    protected array $repositories = [
        PlaceRepository::class => EloquentPlaceRepository::class,
    ];

    protected array $commandHandlers = [
        CreatePlaceCommandHandler::class,
    ];

    protected array $queryHandlers = [
        AllPlacesQueryHandler::class,
    ];

    public function loadRoutes(Router $router): void
    {
        RateLimiter::for(
            'place',
            fn (Request $request) =>
                Limit::perMinute(60)->by($request->user()?->id ?: $request->ip())
        );

        $router
            ->middleware(['api', 'throttle:place'])
            ->as('api.places.')
            ->prefix('api/places')
            ->group(function (Router $router) {
                $router
                    ->get('/', AllPlacesController::class)
                    ->name('index');

                $router
                    ->post('/',CreatePlaceController::class)
                    ->name('store');
            });
    }
}
