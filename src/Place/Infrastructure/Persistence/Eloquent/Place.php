<?php

declare(strict_types=1);

namespace Acme\Place\Infrastructure\Persistence\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin Builder
 */
final class Place extends Model
{
    use HasFactory;

    protected $keyType = 'string';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;

    protected static function newFactory(): PlaceFactory
    {
        return PlaceFactory::new();
    }
}
