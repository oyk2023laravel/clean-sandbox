<?php

declare(strict_types=1);

namespace Acme\Place\Infrastructure\Persistence\Eloquent;

use Acme\Place\Domain\Place;
use Acme\Place\Domain\PlaceId;
use Acme\Place\Domain\PlaceNotFound;
use Acme\Place\Domain\PlaceRepository as PlaceRepositoryInterface;
use Acme\Place\Domain\Places;
use Acme\Place\Infrastructure\Persistence\Eloquent\Place as Model;
use Exception;
use Illuminate\Support\Facades\DB;
use Shared\Infrastructure\Persistence\Eloquent\Exceptions\EloquentException;

final class PlaceRepository implements PlaceRepositoryInterface
{
    public function __construct(private Model $model) {}

    public function find(PlaceId $id): ?Place
    {
        $place = $this->model->find($id->value());

        if ($place === null) {
            return null;
        }

        return $this->toDomain($place);
    }

    public function all(): Places
    {
        $places = $this->model->all()->map(
            fn (Model $model) => $this->toDomain($model)
        )->toArray();

        return new Places($places);
    }

    /**
     * @throws EloquentException
     */
    public function save(Place $place): void
    {
        $model = $this->model->find($place->id()->value());

        if ($model === null) {
            $model = new Model;
            $model->setAttribute('id', $place->id()->value());
        }

        $model->setAttribute('name', $place->name()->value());

        DB::beginTransaction();
        try {
            $model->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw new EloquentException(
                $e->getMessage(),
                $e->getCode(),
                $e->getPrevious()
            );
        }
    }

    /**
     * @throws PlaceNotFound
     */
    public function delete(PlaceId $id): void
    {
        $place = $this->model->find($id->value());

        if ($place === null) {
            throw new PlaceNotFound();
        }

        $place->delete();
    }

    private function toDomain(Model $model): Place
    {
        return Place::from(
            $model->getAttribute('id'),
            $model->getAttribute('name')
        );
    }
}
