<?php

declare(strict_types=1);

namespace Acme\Place\Infrastructure\Persistence\Eloquent;

use Illuminate\Database\Eloquent\Factories\Factory;

final class PlaceFactory extends Factory
{
    protected $model = Place::class;

    /** @noinspection PhpArrayShapeAttributeCanBeAddedInspection */
    public function definition(): array
    {
        return [
            'id' => $this->faker->unique()->uuid,
            'name' => $this->faker->word
        ];
    }
}
