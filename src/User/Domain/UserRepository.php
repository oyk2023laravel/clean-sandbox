<?php

declare(strict_types=1);

namespace Acme\User\Domain;

interface UserRepository
{
    public function find(UserId $id): ?User;

    public function save(User $user): void;
}
