<?php

declare(strict_types=1);

namespace Acme\User\Domain;

use Shared\Domain\ValueObject\StringValueObject;

final class UserName extends StringValueObject
{
}
