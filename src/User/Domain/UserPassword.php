<?php

declare(strict_types=1);

namespace Acme\User\Domain;

use Shared\Domain\ValueObject\SecretStringValueObject;

final class UserPassword extends SecretStringValueObject
{
}
