<?php

declare(strict_types=1);

namespace Acme\User\Domain;

use Shared\Domain\ValueObject\DateTimeValueObject;

final class UserEmailVerifiedAt extends DateTimeValueObject
{
}
