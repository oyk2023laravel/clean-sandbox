<?php

declare(strict_types=1);

namespace Acme\User\Domain;

use Shared\Domain\ValueObject\UuidValueObject;

final class UserId extends UuidValueObject
{
}
