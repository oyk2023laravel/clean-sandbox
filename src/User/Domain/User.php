<?php

declare(strict_types=1);

namespace Acme\User\Domain;

use DateTimeImmutable;
use JetBrains\PhpStorm\Pure;
use Shared\Domain\Aggregate\AggregateRoot;

final class User extends AggregateRoot
{
    public function __construct(
        public readonly UserId $id,
        public readonly UserName $name,
        public readonly UserEmail $email,
        public readonly UserPassword $password,
        public readonly ?UserEmailVerifiedAt $emailVerifiedAt = null
    ) {}

    public static function from(
        string $id,
        string $name,
        string $email,
        string $password,
        ?DateTimeImmutable $emailVerifiedAt = null
    ): self
    {
        return new self(
            UserId::from($id),
            UserName::from($name),
            UserEmail::from($email),
            UserPassword::from($password),
            $emailVerifiedAt ? UserEmailVerifiedAt::from($emailVerifiedAt) : null
        );
    }

    #[Pure]
    public static function create(
        UserId $id,
        UserName $name,
        UserEmail $email,
        UserPassword $password,
        ?UserEmailVerifiedAt $emailVerifiedAt = null
    ): self
    {
        return new self($id, $name, $email, $password, $emailVerifiedAt);
    }
}
