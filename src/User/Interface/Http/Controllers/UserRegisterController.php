<?php

declare(strict_types=1);

namespace Acme\User\Interface\Http\Controllers;

use Acme\User\Application\Register\RegisterUserCommand;
use Acme\User\Interface\Http\Requests\UserRegisterRequest;
use Illuminate\Http\JsonResponse;
use Shared\Interface\Http\Controllers\Controller;

final class UserRegisterController extends Controller
{
    public function __invoke(UserRegisterRequest $request): JsonResponse
    {
        $id = $this->generateUuid();

        $this->dispatchCommand(
            new RegisterUserCommand(
                $id,
                $request->get('name'),
                $request->get('email'),
                $request->get('password')
            )
        );

        return response()->json(['user' => ['id' => $id]], 201);
    }
}
