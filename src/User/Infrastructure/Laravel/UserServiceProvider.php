<?php

declare(strict_types=1);

namespace Acme\User\Infrastructure\Laravel;

use Acme\User\Application\Register\RegisterUserCommandHandler;
use Acme\User\Domain\UserRepository;
use Acme\User\Infrastructure\Persistence\Eloquent\UserRepository as EloquentUserRepository;
use Acme\User\Interface\Http\Controllers\UserRegisterController;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\RateLimiter;
use Shared\Infrastructure\Laravel\DomainServiceProvider;
use Shared\Infrastructure\Laravel\HasMigrations;
use Shared\Infrastructure\Laravel\HasRoutes;

final class UserServiceProvider extends DomainServiceProvider
{
    use HasRoutes, HasMigrations;

    protected array $repositories = [
        UserRepository::class => EloquentUserRepository::class,
    ];

    protected array $commandHandlers = [
        RegisterUserCommandHandler::class,
    ];

    public function loadRoutes(Router $router): void
    {
        $router
            ->middleware(['api', 'throttle:user'])
            ->as('api.users.')
            ->prefix('api/users')
            ->group(function (Router $router) {
                $router
                    ->post('register',UserRegisterController::class)
                    ->name('store');
            });
    }
}
