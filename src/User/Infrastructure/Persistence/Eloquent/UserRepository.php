<?php

declare(strict_types=1);

namespace Acme\User\Infrastructure\Persistence\Eloquent;

use Acme\User\Domain\User;
use Acme\User\Domain\UserId;
use Acme\User\Domain\UserRepository as UserRepositoryInterface;
use Acme\User\Infrastructure\Persistence\Eloquent\User as Model;
use Exception;
use Illuminate\Support\Facades\DB;
use Shared\Infrastructure\Persistence\Eloquent\Exceptions\EloquentException;

final class UserRepository implements UserRepositoryInterface
{
    public function __construct(
        private readonly Model $model
    ) {}

    public function find(UserId $id): ?User
    {
       $user = $this->model->find($id->value());

       if ($user === null) {
           return null;
       }

       return $this->toDomain($user);
    }

    /**
     * @throws EloquentException
     */
    public function save(User $user): void
    {
        $model = $this->model->find($user->id->value());

        if ($model === null) {
            $model = new Model;
            $model->setAttribute('id', $user->id->value());
        }

        $model->setAttribute('name', $user->name->value());
        $model->setAttribute('email', $user->email->value());
        $model->setAttribute('password', $user->password->value());
        $model->setAttribute('email_verified_at', $user->emailVerifiedAt?->value());

        DB::beginTransaction();
        try {
            $model->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw new EloquentException(
                $e->getMessage(),
                $e->getCode(),
                $e->getPrevious()
            );
        }
    }

    private function toDomain(Model $model): User
    {
        return User::from(
            $model->getAttribute('id'),
            $model->getAttribute('name'),
            $model->getAttribute('email'),
            $model->getAttribute('password'),
            $model->getAttribute('email_verified_at')
        );
    }
}
