<?php

declare(strict_types=1);

namespace Acme\User\Infrastructure\Persistence\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @mixin Builder
 */
final class User extends Authenticatable
{
    use HasFactory;

    protected $keyType = 'string';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;

    protected static function newFactory()
    {
        return UserFactory::new();
    }
}
