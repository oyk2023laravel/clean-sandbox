<?php

declare(strict_types=1);

namespace Acme\User\Application\Register;

use Acme\User\Domain\User;
use Acme\User\Domain\UserAlreadyExists;
use Acme\User\Domain\UserId;
use Acme\User\Domain\UserRepository;
use Shared\Domain\Bus\Command\CommandHandler;
use Shared\Domain\Bus\Event\EventBus;

final class RegisterUserCommandHandler implements CommandHandler
{
    public function __construct(
        private UserRepository $repository,
        private EventBus $bus
    ) {}

    /**
     * @throws UserAlreadyExists
     */
    public function __invoke(RegisterUserCommand $command): void
    {
        $id = UserId::from($command->id);
        $user = $this->repository->find($id);

        if ($user !== null) {
            throw new UserAlreadyExists();
        }

        $user = User::from(
            $command->id,
            $command->name,
            $command->email,
            $command->password
        );

        $this->repository->save($user);
        $this->bus->publish(...$user->pullDomainEvents());
    }
}
