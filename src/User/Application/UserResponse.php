<?php

declare(strict_types=1);

namespace Acme\User\Application;

use Acme\User\Domain\User;
use JetBrains\PhpStorm\Pure;
use Shared\Domain\Bus\Query\Response;

final class UserResponse implements Response
{
    public function __construct(
        public readonly string $id,
        public readonly string $name,
        public readonly string $email
    ) {}

    #[Pure]
    public static function from(User $user): self
    {
        return new self(
            $user->id->value(),
            $user->name->value(),
            $user->email->value()
        );
    }

    /** @noinspection PhpArrayShapeAttributeCanBeAddedInspection */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
        ];
    }
}
