<?php

declare(strict_types=1);

namespace Shared\Interface\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Shared\Infrastructure\Laravel\AsksQuery;
use Shared\Infrastructure\Laravel\DispatchesCommand;
use Shared\Infrastructure\Laravel\GeneratesUuid;
use Shared\Infrastructure\Laravel\PublishesEvent;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, GeneratesUuid, DispatchesCommand, AsksQuery, PublishesEvent;
}
