<?php

declare(strict_types=1);

namespace Shared\Infrastructure\Laravel;

use Shared\Domain\Bus\Query\Query;
use Shared\Domain\Bus\Query\QueryBus;
use Shared\Domain\Bus\Query\Response;

trait AsksQuery
{
    public function askQuery(Query $query): Response
    {
        return app(QueryBus::class)->ask($query);
    }
}
