<?php

declare(strict_types=1);

namespace Shared\Infrastructure\Laravel;

use Illuminate\Contracts\Console\Kernel as LaravelConsoleKernel;
use Illuminate\Contracts\Debug\ExceptionHandler as LaravelExceptionHandler;
use Illuminate\Contracts\Http\Kernel as LaravelHttpKernel;
use Illuminate\Foundation\Application as Laravel;
use Shared\Application\Exceptions\Handler as AcmeExceptionHandler;
use Shared\Interface\Console\Kernel as AcmeConsoleKernel;
use Shared\Interface\Http\Kernel as AcmeHttpKernel;

final class Application extends Laravel
{
    protected $namespace = 'Acme';

    public function __construct($basePath = null)
    {
        parent::__construct($basePath);

        $this->singleton(
            LaravelHttpKernel::class,
            AcmeHttpKernel::class
        );

        $this->singleton(
            LaravelConsoleKernel::class,
            AcmeConsoleKernel::class
        );

        $this->singleton(
            LaravelExceptionHandler::class,
            AcmeExceptionHandler::class
        );
    }
}
