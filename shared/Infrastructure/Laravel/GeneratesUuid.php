<?php

declare(strict_types=1);

namespace Shared\Infrastructure\Laravel;

use Shared\Domain\UuidGenerator;

trait GeneratesUuid
{
    public function generateUuid(): string
    {
        return app(UuidGenerator::class)->generate();
    }
}
