<?php

declare(strict_types=1);

namespace Shared\Infrastructure\Laravel;

use Illuminate\Routing\Router;

trait HasRoutes
{
    public function registerRoutes(): void
    {
        if ($this->app->routesAreCached()) {
            $this->app->booted(function () {
                require $this->app->getCachedRoutesPath();
            });
        } else {
            $this->app->call([$this, 'loadRoutes']);

            $this->app->booted(function () {
                $this->app['router']->getRoutes()->refreshNameLookups();
                $this->app['router']->getRoutes()->refreshActionLookups();
            });
        }
    }

    public function loadRoutes(Router $router): void {}

    public function register(): void
    {
        parent::register();

        $this->booted(
            fn () => $this->registerRoutes()
        );
    }
}
