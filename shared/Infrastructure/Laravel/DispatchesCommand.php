<?php

declare(strict_types=1);

namespace Shared\Infrastructure\Laravel;

use Shared\Domain\Bus\Command\Command;
use Shared\Domain\Bus\Command\CommandBus;

trait DispatchesCommand
{
    public function dispatchCommand(Command $command): void
    {
        app(CommandBus::class)->dispatch($command);
    }
}
