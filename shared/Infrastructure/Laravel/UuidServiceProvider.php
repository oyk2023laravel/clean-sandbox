<?php

declare(strict_types=1);

namespace Shared\Infrastructure\Laravel;

use Illuminate\Support\ServiceProvider;
use Shared\Domain\UuidGenerator;
use Shared\Infrastructure\RamseyUuidGenerator;

class UuidServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(
            UuidGenerator::class,
            RamseyUuidGenerator::class
        );
    }
}
