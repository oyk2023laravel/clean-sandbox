<?php

declare(strict_types=1);

namespace Shared\Infrastructure\Laravel;

use Illuminate\Support\ServiceProvider;
use Shared\Domain\Bus\Command\CommandBus;
use Shared\Domain\Bus\Event\EventBus;
use Shared\Domain\Bus\Query\QueryBus;
use Shared\Infrastructure\Bus\Messenger\MessengerCommandBus;
use Shared\Infrastructure\Bus\Messenger\MessengerEventBus;
use Shared\Infrastructure\Bus\Messenger\MessengerQueryBus;

class BusServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(
            EventBus::class,
            function ($app) {
                return new MessengerEventBus($app->tagged('event_subscriber'));
            }
        );

        $this->app->bind(
            QueryBus::class,
            function ($app) {
                return new MessengerQueryBus($app->tagged('query_handler'));
            }
        );

        $this->app->bind(
            CommandBus::class,
            function ($app) {
                return new MessengerCommandBus($app->tagged('command_handler'));
            }
        );
    }
}
