<?php

declare(strict_types=1);

namespace Shared\Infrastructure\Laravel;

use Shared\Domain\Bus\Event\DomainEvent;
use Shared\Domain\Bus\Event\EventBus;

trait PublishesEvent
{
    public function publishEvent(DomainEvent ...$events): void
    {
        app(EventBus::class)->publish($events);
    }
}
