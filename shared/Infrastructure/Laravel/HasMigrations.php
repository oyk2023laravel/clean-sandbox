<?php

declare(strict_types=1);

namespace Shared\Infrastructure\Laravel;

trait HasMigrations
{
    public function boot()
    {
        $this->loadMigrationsFrom($this->path('Infrastructure/Persistence/Migrations'));
    }
}
