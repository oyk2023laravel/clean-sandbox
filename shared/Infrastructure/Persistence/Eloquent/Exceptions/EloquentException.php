<?php

declare(strict_types=1);

namespace Shared\Infrastructure\Persistence\Eloquent\Exceptions;

use Shared\Infrastructure\Exceptions\InfrastructureException;

final class EloquentException extends InfrastructureException {}
