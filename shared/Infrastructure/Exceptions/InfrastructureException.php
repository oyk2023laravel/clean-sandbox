<?php

namespace Shared\Infrastructure\Exceptions;

use Exception;

abstract class InfrastructureException extends Exception {}
