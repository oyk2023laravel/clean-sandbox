<?php

declare(strict_types=1);

namespace Shared\Infrastructure\Bus\Exceptions;

use JetBrains\PhpStorm\Pure;
use Shared\Infrastructure\Exceptions\InfrastructureException;
use Throwable;

class QueryNotRegistered extends InfrastructureException
{
    #[Pure]
    public function __construct(string $message = "Query not registered", int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
