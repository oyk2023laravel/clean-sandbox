<?php

declare(strict_types=1);

namespace Shared\Domain\Collection;

interface CollectionInterface
{
    public function all(): array;
}
