<?php

declare(strict_types=1);

namespace Shared\Domain\ValueObject;

use DateTimeImmutable;
use JetBrains\PhpStorm\Pure;

abstract class DateTimeValueObject
{
    public function __construct(protected DateTimeImmutable $value)
    {}

    #[Pure]
    public static function from(DateTimeImmutable $value): static
    {
        return new static($value);
    }

    public function value(): DateTimeImmutable
    {
        return $this->value;
    }
}
