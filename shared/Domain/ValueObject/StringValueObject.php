<?php

declare(strict_types=1);

namespace Shared\Domain\ValueObject;

use JetBrains\PhpStorm\Pure;

abstract class StringValueObject
{
    public function __construct(protected string $value)
    {
    }

    #[Pure]
    public static function from(string $value): static
    {
        return new static($value);
    }

    public function value(): string
    {
        return $this->value;
    }
}
