<?php

declare(strict_types=1);

namespace Shared\Domain\ValueObject;

use InvalidArgumentException;
use Ramsey\Uuid\Uuid;

class UuidValueObject
{
    public function __construct(protected string $value)
    {
        $this->assertIsValidUuid($value);
    }

    public static function random(): static
    {
        return new static(Uuid::uuid4()->toString());
    }

    public static function from(string $value): static
    {
        return new static($value);
    }

    public function value(): string
    {
        return $this->value;
    }

    private function assertIsValidUuid(string $id): void
    {
        if (! Uuid::isValid($id)) {
            throw new InvalidArgumentException(sprintf('`<%s>` does not allow the value `<%s>`.', static::class, $id));
        }
    }
}
