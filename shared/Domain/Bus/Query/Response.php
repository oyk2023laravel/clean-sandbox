<?php

namespace Shared\Domain\Bus\Query;

use Illuminate\Contracts\Support\Arrayable;

interface Response extends Arrayable
{
}
