<?php

declare(strict_types=1);

namespace Shared\Domain\Bus\Event;

use DateTimeImmutable;
use Shared\Domain\ValueObject\UuidValueObject;

abstract class DomainEvent
{
    public function __construct(
        private string $id,
        private ?string $eventId = null,
        private ?string $occurredOn = null
    ) {
        $this->eventId = $eventId ?? UuidValueObject::random()->value();
        $this->occurredOn = $this->occurredOn ?? (new DateTimeImmutable())->format('Y-m-d H:i:s.u T');
    }

    abstract public static function from(
        string $id,
        array $body,
        string $eventId,
        string $occurredOn
    ): self;

    abstract public static function eventName(): string;

    abstract public function to(): array;

    public function id(): string
    {
        return $this->id;
    }

    public function eventId(): string
    {
        return $this->eventId;
    }

    public function occurredOn(): string
    {
        return $this->occurredOn;
    }
}
