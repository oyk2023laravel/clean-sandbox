<?php

declare(strict_types=1);

namespace Shared\Domain\Exceptions;

use Exception;

abstract class DomainException extends Exception
{
}
